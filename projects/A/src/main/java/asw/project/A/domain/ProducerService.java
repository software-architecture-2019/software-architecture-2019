package asw.project.A.domain;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ProducerService {

	private static final Logger logger = Logger.getLogger(ProducerService.class.toString());
	
    @Value("${animals}") 
	private String animals;
	
	@Autowired
	private MessagePublisher messagePublisher;
	
    public String publish(String message) {
		logger.info("PUBLISHING MESSAGE: " + message);
		return messagePublisher.publish(message);  
	}
	
	public String getRandomAnimal() {
		/* restituisce una parola a caso tra le parole di questo tipo */ 
		String[] wordArray = animals.split(", ");
		int i = (int) (Math.round(Math.random()*(wordArray.length-1)));
		String word = wordArray[i];
		return word; 
	}
}
