package asw.project.A.rest;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import asw.project.A.client.impl.CServiceClient;
import asw.project.A.domain.ProducerService;

@RestController
public class AController 
{
	private static final Logger logger = Logger.getLogger(AController.class.toString());
	
	@Autowired 
    private ProducerService producerService;
	
	@Autowired
	CServiceClient cServiceClient;

	@Value("${instanceName}") 
	private String instanceName;
	
	/***
	 * Inoltra una richiesta GET al servizio C
	 * @return una lista dei dati in memoria di C
	 */
	@GetMapping("/get")
	public String get() 
	{
		String result = new String();
		
		logger.info("Received GET request on the path /get: ");
		result = cServiceClient.getList();
		
		return result;
	}
	
	/***
	 * Alla ricezione di una POST su path /post, invia un messaggio al canale
	 */
	@PostMapping("/post")
	public String post() 
	{
		logger.info("Received POST request on the path /post: ");
		String randomAnimal = this.producerService.getRandomAnimal();
		return producerService.publish(instanceName + ": " + randomAnimal);
	}
	
}
