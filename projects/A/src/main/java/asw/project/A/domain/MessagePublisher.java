package asw.project.A.domain;

public interface MessagePublisher {
	
	public String publish(String message);

}
