package asw.project.A.messagepublisher;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import asw.project.A.domain.MessagePublisher;

@Component 
public class MessagePublisherImpl implements MessagePublisher {

	private Logger logger = Logger.getLogger(MessagePublisherImpl.class.toString());

	@Value("${asw.kafka.channel.out}")
	private String channel;

    @Autowired
    private KafkaTemplate<String, String> template;

    @Override
    public String publish(String message) {
        // logger.info("PUBLISHING MESSAGE: " + message + " ON CHANNEL: " + channel);
        template.send(channel, message);
        // template.flush();
        return "Publishing message: [" + message + "]";
    }

}
