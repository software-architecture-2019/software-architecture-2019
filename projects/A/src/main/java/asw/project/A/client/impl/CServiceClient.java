package asw.project.A.client.impl;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asw.project.A.client.CService;
import asw.project.A.client.CarloService;
import asw.project.A.client.CristianaService;

@Service
public class CServiceClient implements CService {

	@Autowired
	private CarloService carloService;
	
	@Autowired
	private CristianaService cristianaService;

	@Override
	public String getList() 
	{
		int rnd = new Random().nextInt(100);  // [0...100] [min = 0, max = 100]
		
		if(rnd > 50)
		{
			return carloService.getList();
		}
		
		return cristianaService.getList();
	}

}
