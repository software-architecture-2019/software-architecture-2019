package asw.project.A.client;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface CService 
{
	@RequestMapping(value="/get", method=RequestMethod.GET)
	public String getList();
}
