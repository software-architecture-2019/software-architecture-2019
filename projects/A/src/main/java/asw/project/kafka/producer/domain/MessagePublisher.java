package asw.project.kafka.producer.domain;

public interface MessagePublisher {
	
	public void publish(String message);
}
