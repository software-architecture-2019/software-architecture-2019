package asw.project.kafka.producer.domain;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.logging.*;

@Service
public class ProducerService{
	
	@Autowired
	private MessagePublisher messagePublisher;
	
	private static final Logger logger = Logger.getLogger(ProducerService.class.toString());
	
	public void publish(String message) {
		
		logger.info("PUBLISHING MESSAGE: " + message);
		this.messagePublisher.publish(message);
	}
}
