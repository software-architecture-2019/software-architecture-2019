package asw.project.kafka.messagepublisher;

import asw.project.kafka.producer.domain.MessagePublisher;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.kafka.core.KafkaTemplate;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

import java.util.logging.Logger;

@Component
public class MessagePublisherImpl implements MessagePublisher {
	
	private Logger logger = Logger.getLogger(MessagePublisherImpl.class.toString());

	@Value("${asw.kafka.channel.out}")
	private String channel;

    @Autowired
    private KafkaTemplate<String, String> template;

    @Override
    public void publish(String message) {
        logger.info("PUBLISHING MESSAGE: " + message + " ON CHANNEL: " + channel);
        template.send(channel, message);
        // template.flush();
    }
}
