package asw.project.C.rest;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import asw.project.C.repository.RepositoryService;

@RestController
public class CController 
{
	private static final Logger logger = Logger.getLogger(CController.class.toString());
	
	 @Autowired
	 private RepositoryService repositoryService;
	
	@GetMapping("/get")
	public String get() 
	{
		logger.info("Received GET request on the path /get: ");
		return this.repositoryService.getAll();
	}

	@PostMapping("/post/{bInstanceName}/{aInstanceName}/{animal}")
	public void post(@PathVariable("bInstanceName") String bInstanceName, @PathVariable("aInstanceName") String aInstanceName, @PathVariable("animal")  String animal) 
	{
		logger.info("Received POST request on the path /post: ");
		this.repositoryService.store(bInstanceName, aInstanceName, animal);
	}

}
