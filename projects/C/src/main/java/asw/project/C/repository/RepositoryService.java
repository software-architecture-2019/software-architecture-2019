package asw.project.C.repository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RepositoryService 
{
	 Map<String, Map<String, List<String>>> map = new HashMap<>();	// Map<bInstanceName, Map<aInstanceName, List<animals>>>
	 
	 @Value("${instanceName}") 
	 private String instanceName;

	public String getAll() 
	{
		String result = new String();
		result = instanceName + ": " + this.map.toString();
		return result;
	}

	public void store(String bInstanceName, String aInstanceName, String animal) 
	{
		Map<String, List<String>> serviceAnimalsKeyValuePairs = new HashMap<>();	
		List<String> animals = new LinkedList<>();
		
		if(map.containsKey(bInstanceName))									// Se un'istanza di b ha gi� memorizzato qualcosa qui
		{
			serviceAnimalsKeyValuePairs = map.get(bInstanceName);			// Retrive della lista di coppie chiave valore delle istanze che hanno visto animali
			
			if(serviceAnimalsKeyValuePairs.containsKey(aInstanceName))		// Se l'istanza del servizio a ha gia' salvato animali visti in quest'istanza
			{
				animals = serviceAnimalsKeyValuePairs.get(aInstanceName);	// Fai retrive di quella lista
			}
		}
		
		animals.add(animal);
		serviceAnimalsKeyValuePairs.put(aInstanceName, animals);
		this.map.put(bInstanceName, serviceAnimalsKeyValuePairs);
	}
}
