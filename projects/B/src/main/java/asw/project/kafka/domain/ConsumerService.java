package asw.project.kafka.domain;

import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class ConsumerService {
	
	
    private Logger logger = Logger.getLogger(ConsumerService.class.toString());

    public void onMessage(String message) {
		logger.info("RECEIVED MESSAGE: " + message);
		//TO-DO: inviare messaggio post a componente C con message ricevuto. :)
	}
}
