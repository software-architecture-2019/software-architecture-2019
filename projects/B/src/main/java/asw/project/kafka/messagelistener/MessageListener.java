package asw.project.kafka.messagelistener;

import asw.project.kafka.domain.ConsumerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Value;

import java.util.logging.Logger;

@Component
public class MessageListener {
	
	private Logger logger = Logger.getLogger(MessageListener.class.toString());

	@Value("${asw.kafka.channel.in}")
	private String channel;

	@Value("${asw.kafka.groupid}")
	private String groupId;

    @Autowired
    private ConsumerService ConsumerService;

	@KafkaListener(topics = "${asw.kafka.channel.in}", groupId="${asw.kafka.groupid}")
    public void listen(ConsumerRecord<String, String> record) throws Exception {
        logger.info("MESSAGE LISTENER: " + record.toString());
        String message = record.value();
		ConsumerService.onMessage(message); 
    }
}
