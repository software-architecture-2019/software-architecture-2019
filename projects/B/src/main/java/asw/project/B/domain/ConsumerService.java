package asw.project.B.domain;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import asw.project.B.client.impl.CServiceClient;

@Service
public class ConsumerService {
	
	private Logger logger = Logger.getLogger(ConsumerService.class.toString());
	
	@Value("${instanceName}")
	private String instanceName;
	
	@Autowired
	private CServiceClient cService;

    public void onMessage(String message) {
		logger.info("RECEIVED MESSAGE: " + message);
		String[] messageParts = message.split(": ");
		cService.post(instanceName, messageParts[0],messageParts[1]);
	}

}
