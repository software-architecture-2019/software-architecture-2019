package asw.project.B.client.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asw.project.B.client.CristianaService;

@Service
public class CristianaServiceImpl implements CristianaService 
{
	@Autowired
	private CristianaService cristianaService;

	@Override
	public void post(String bInstanceName, String aInstanceName, String animal) {
		cristianaService.post(bInstanceName, aInstanceName, animal);		
	}
}
