package asw.project.B.rest;

import java.util.logging.Logger;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BController {
	
	private final Logger logger = Logger.getLogger(BController.class.toString());
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public String get()
	{
		logger.info
		(
				"[" + RequestMethod.GET + "] " +
				BController.class.toString() + "." +
				new Throwable().getStackTrace()[0].getMethodName()
		);
		
		return 
				"[" + RequestMethod.GET + "] " +
				BController.class.toString() + "." +
				new Throwable().getStackTrace()[0].getMethodName();
	}
	
	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String post()
	{
		logger.info
		(
				"[" + RequestMethod.POST + "] " +
						BController.class.toString() + "." +
				new Throwable().getStackTrace()[0].getMethodName()
		);
		
		return "[" + RequestMethod.POST + "] " +
				BController.class.toString() + "." +
				new Throwable().getStackTrace()[0].getMethodName();
	}
}
