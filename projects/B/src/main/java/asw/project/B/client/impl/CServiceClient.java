package asw.project.B.client.impl;

import java.util.Random;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asw.project.B.client.CristianaService;
import asw.project.B.client.CarloService;
import asw.project.B.client.CService;

@Service
public class CServiceClient implements CService 
{

	private static final Logger logger = Logger.getLogger(CServiceClient.class.toString());
	
	@Autowired
	private CarloService carloService;
	
	@Autowired
	private CristianaService cristianaService;
	
	@Override
	public void post(String bInstanceName, String aInstanceName, String animal)
	{
		int rnd = new Random().nextInt(100);  // [0...100] [min = 0, max = 100]
		
		if(rnd > 50)
		{
			logger.info("Sending REST call POST to Carlo: [" + bInstanceName + ": " + aInstanceName + ": " + animal + "]");
			carloService.post(bInstanceName, aInstanceName, animal);
			return;
		}
		
		logger.info("Sending REST call POST to Cristiana: [" + bInstanceName + ": " + aInstanceName + ": " + animal + "]");
		cristianaService.post(bInstanceName, aInstanceName, animal);
		
	}
}
