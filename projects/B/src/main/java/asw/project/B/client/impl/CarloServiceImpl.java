package asw.project.B.client.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import asw.project.B.client.CarloService;

@Service
public class CarloServiceImpl implements CarloService 
{
	@Autowired
	private CarloService carloService;

	@Override
	public void post(String bInstanceName, String aInstanceName, String animal) {
		carloService.post(bInstanceName, aInstanceName, animal);
	}
}
