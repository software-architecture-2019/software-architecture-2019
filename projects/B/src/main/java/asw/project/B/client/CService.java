package asw.project.B.client;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface CService 
{
	@RequestMapping(value="/post/{bInstanceName}/{aInstanceName}/{animal}", method=RequestMethod.POST)
	public void post(@PathVariable("bInstanceName")String bInstanceName, @PathVariable("aInstanceName") String aInstanceName, @PathVariable("animal") String animal);
}
