# Progetto per il corso di Architetture software di Roma Tre

Questo repository  contiene il codice del progetto proposto dal docente del corso nell'anno accademico 2018/2019
Il progetto scelto � il "Progetto 2D".

Tale progetto prevede la realizzazione di una semplice applicazione distribuita composta
da una terna di servizi A, B e C, che comunicano tra loro tramite invocazioni remote con REST e
comunicazione asincrona con Kafka. Per ciascun servizio possono esserci pi� istanze in esecuzione,
ciascuna con un proprio nome. Questi servizi possono essere mandati in esecuzione su Docker.

## Esecuzione
Questa applicazione pu� essere costruita ed eseguita nell'ambiente di esecuzione workstation, sul nodo workstation.
Per eseguire l'applicazione, pu� essere eseguito lo script runner.sh che contiene gli script per il clean e la build Java, per la build e il push delle immagini Docker e lo script per avviare l'applicazione in uno swarm docker.

L'applicazione espone un servizio sulla porta 8080 del nodo workstation ed � possibile verificarlo usando gli script get-client.sh e post-client.sh


## Installazione e requisiti (stabili)
## Installazione e requisiti
Le versioni indicate sono quelle utilizzate durante lo sviluppo, si consiglia quindi di utilizzare le versioni indicate.

* [Gradle 5.4.1](https://gradle.org/)
* Java 1.8
* Java 1.8
* [Vagrant 2.2.4](https://www.vagrantup.com/)
* [Oracle VirtualBox 6.0.8](https://www.virtualbox.org/)

